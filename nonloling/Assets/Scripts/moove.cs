﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moove : MonoBehaviour
{
    public float speed=0.2f;
    void Update()
    {
        float moveZ = Input.GetAxis ("Vertical");
        float moveX = Input.GetAxis("Horizontal");
        transform.Translate(moveX*speed,0,moveZ*speed);
    }
}
